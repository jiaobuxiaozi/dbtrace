package john.walker.log;

/**
 * @author 30san
 *
 */
public abstract class AbstractLogger implements ILog {

	private transient int level;

	public AbstractLogger(){
		this.level = ILog.INFO;
	}

	@Override
	public void log(String msg) {
		switch (level) {
		case ILog.DEBUG:
			debug(msg);
			break;
		case ILog.INFO:
			info(msg);
			break;
		case ILog.WARN:
			warn(msg);
			break;
		case ILog.ERROR:
			error(msg);
			break;
		case ILog.FATAL:
			fatal(msg);
		default:
			;
		}
	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public void setLevel(int level) {
		if (level < ILog.DEBUG) {
			this.level = ILog.DEBUG;
		} else if (level > ILog.FATAL) {
			this.level = ILog.FATAL;
		} else {
			this.level = level;
		}
	}
}
